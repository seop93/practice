package com.hello.practice.controller;

import com.hello.practice.domain.Board;
import com.hello.practice.domain.dto.BoardSavedDto;
import com.hello.practice.domain.dto.BoardSearchCond;
import com.hello.practice.domain.dto.BoardUpdateDto;
import com.hello.practice.service.BoardService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/boards")
@RequiredArgsConstructor
@Slf4j
public class BoardController {
    private final BoardService boardService;

//    @GetMapping
//    public String items(@ModelAttribute("boarSearch") BoardSearchCond boardSearch, Model model) {
//        List<Board> boards = boardService.findBoards(boardSearch);
//        model.addAttribute("boards", boards);
//        return "boards";
//

    @GetMapping
    public String boards(@ModelAttribute("boardSearch") BoardSearchCond boardSearch, Model model) {
        List<Board> boards = boardService.findBoards();
        model.addAttribute("boards", boards);
        return "boards";
    }

    @GetMapping("/{boardId}")
    public String item(@PathVariable long boardId, Model model) {
        Board board = boardService.findById(boardId).get();
        model.addAttribute("board", board);
        return "board";
    }

    @GetMapping("/add")
    public String addForm() {
        return "addForm";
    }

    @PostMapping("/add")
    public String addBoard(BoardSavedDto dto, RedirectAttributes redirectAttributes) {
        log.info("{}{}",dto.getBoardName(), dto.getDetail());
        Board savedBoard = boardService.save(dto);
        redirectAttributes.addAttribute("boardId", savedBoard.getId());
        redirectAttributes.addAttribute("status", true);
        return "redirect:/boards/{boardId}";
    }

    @GetMapping("/{boardId}/edit")
    public String editForm(@PathVariable Long boardId, Model model) {
        Optional<Board> optionalBoard = boardService.findById(boardId);
        optionalBoard.ifPresent(board -> model.addAttribute("board", board));
        return "editForm";
    }

    @PostMapping("/{boardId}/edit")
    public String edit(@PathVariable Long boardId, @ModelAttribute BoardUpdateDto updateParam) {
        boardService.update(boardId, updateParam);
        return "redirect:/boards/{boardId}";
    }

}
