package com.hello.practice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


public class HomeController {

    @RequestMapping("/")
    public String home() {
        return "redirect:/boards";
    }

    @GetMapping("/jubi")
    @ResponseBody
    public String test() {
        return "jubi is love";
    }

}
