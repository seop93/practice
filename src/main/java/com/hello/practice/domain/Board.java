package com.hello.practice.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Board {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String boardName;

    private String detail;

    public static Board createBoard(String boardName, String detail){
        return Board.builder()
                .boardName(boardName)
                .detail(detail)
                .build();
    }

    public void updateBoard(String boardName, String detail){
        this.boardName = boardName;
        this.detail = detail;
    }

}
