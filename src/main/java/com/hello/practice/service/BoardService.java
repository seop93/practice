package com.hello.practice.service;

import com.hello.practice.domain.Board;
import com.hello.practice.domain.dto.BoardSavedDto;
import com.hello.practice.domain.dto.BoardUpdateDto;
import com.hello.practice.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class BoardService {

    private final BoardRepository boardRepository;


    public Board save(BoardSavedDto dto) {
        Board board = Board.createBoard(dto.getBoardName(), dto.getDetail());
        boardRepository.save(board);
        return board;
    }

    public void update(Long boardId, BoardUpdateDto updateParam) {
        Board findBoard = boardRepository.findById(boardId).orElseThrow();
//        findBoard.ifPresent(board -> {
//            board.setBoardName(updateParam.getBoardName());
//            board.setDetail(updateParam.getDetail());
//            boardRepository.save(board);
//        })
        findBoard.updateBoard(updateParam.getBoardName(), updateParam.getDetail());
        boardRepository.save(findBoard);

    }

    public Optional<Board> findById(Long id) {
        return boardRepository.findById(id);
    }


    public List<Board> findBoards() {
        return boardRepository.findAllBoard();
    }
}
