package com.hello.practice.repository;

import com.hello.practice.domain.Board;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface BoardRepository extends JpaRepository<Board,Long>, BoardRepositoryCustom {
    List<Board> findByDetail(String detail);

}
