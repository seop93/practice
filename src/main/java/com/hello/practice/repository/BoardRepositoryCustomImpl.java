package com.hello.practice.repository;

import com.hello.practice.domain.Board;
import com.hello.practice.domain.QBoard;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;

import java.util.List;


@RequiredArgsConstructor
public class BoardRepositoryCustomImpl implements BoardRepositoryCustom {

    private final JPAQueryFactory em;
    @Override
    public List<Board> findAllBoard() {
        QBoard board = QBoard.board;
        return em.selectFrom(board)
                .orderBy(board.id.desc())
                .fetch();
    }
}
