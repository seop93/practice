package com.hello.practice.repository;

import com.hello.practice.domain.Board;

import java.util.List;

public interface BoardRepositoryCustom {
    List<Board> findAllBoard();

}
